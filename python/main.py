import json
import socket
import sys
import math
import fitting
import threading
import time

class BaseBot(object):
	_socket = None
	_name = None
	_key = None
	_color = None
	
	_gameTick = 0
	_lanes = None
	_pieces = None
	_cars = None
	_track = None
	_physics = None
	_carPhysics = None
	_pursuer = None
	_target = None
	
	_throttle = 0.0
	_switch = 0
	_gameOn = False
	_outOfTrack = False
	
	_frictionAdjusted = None
	_dataCollected = False
	_qualificationRound = False
	
	_gotTurbo = False
	_turboTime = 0
	_turboTicks = 0
	_turboFactor = 1.0
	
	_bendVelocity = None
	
	_messageStack = None
	
	def __init__(self, socket, name, key):
		self._socket = socket
		self._name = name
		self._key = key
		
		self._messageStack = []
	
	###### Low-level functions ######
	
	def _Do(self, type, data):
		self._Send(json.dumps({'msgType': type, 'data': data}))
	
	def _Send(self, msg):
		self._socket.send(msg + '\n')
	
	def _PutOnStack(self, priority, type, data):
		self._messageStack.append( (priority, type, data) )
	
	def _ProcessStack(self):
		if len(self._messageStack):
			self._messageStack.sort()
			priority, type, data = self._messageStack[0]
			if type == 'throttle':
				self._throttle = data
				#print 'throttle: ' + str(data)
			elif type == 'turbo':
				print(data)
				self._physics.SetPowerFactor(self._turboFactor)
				self._gotTurbo = False
			elif type == 'switchLane':
				self._switch = data
				if data < 0:					# This is a little cumbersome...
					data = 'Left'
				elif data > 0:
					data = 'Right'
				else:
					pass
			
			self._Do(type, data)
			self._messageStack = []
	
	###### Event handlers ######
	
	def _OnJoin(self, data):
		print('Joined')
		self.Ping()
	
	def _OnYourCar(self, data):
		self._color = data['color']
		print('I got ' + self._color + ' color')
		self.Ping()
	
	def _OnGameInit(self, data):
		print('Game initialized')
		print('Racing on track: ' + data['race']['track']['name'])
		
		# Unpack the race data to objects
		self._lanes = lanes = []
		for laneData in data['race']['track']['lanes']:
			lane = Lane(laneData)
			lanes.append(lane)
		
		self._pieces = pieces = []
		for pieceData in data['race']['track']['pieces']:
			piece = Piece(pieceData, lanes)
			self._pieces.append(piece)
		
		self._track = track = Track(pieces, lanes)
		
		self._cars = {}
		self._carPhysics = physics = {}
		for carData in data['race']['cars']:
			car = Car(carData)
			self._cars[car.color] = car
			physics[car.color] = PhysicsAnalysis(track, car)
			physics[car.color].CollectFittingData(False)
		
		if not self._physics:									# This is a little hack-ish...
			self._physics = physics[self._color]
			self._physics.CollectFittingData(True)
		else:
			# Flush the collected data
			self._physics.Flush()
			physics[self._color] = self._physics
			
			# Clear the flags that might have been set
			self._outOfTrack = False
			self._gotTurbo = False
			self._switch = 0
			self._target = None
			self._pursuer = None
			self._bendVelocity = None
		
		self._collision = CollisionAnalysis(self._color, physics, track)
		
		if 'laps' in data['race']['raceSession'].keys():
			self._numberOfLaps = data['race']['raceSession']['laps']
			self._qualificationRound = False
		else:
			self._numberOfLaps = 3
			self._qualificationRound = True
		
		if 'quickRace' in data['race']['raceSession'].keys():
			self._isQuickRace = data['race']['raceSession']['quickRace']
		else:
			self._quickRace = False
		
		self.Ping()
	
	def _OnGameStart(self, data):
		print('Race started')
		self._gameOn = True
		self.Ping()
	
	def _OnCarPositions(self, data):		# Analyze the data for slipping and possible tactical lane switching
		carPositions = {}
		for posData in data:
			car = self._cars[posData['id']['color']]
			pos = PositionData(car, posData)
			carPositions[pos.color] = pos
		
		myCar = carPositions[self._color]
		
		if self._outOfTrack:
			self.Ping()				# Maybe I should analyze some data while off-track?
			return
		
		currentTick = self._gameTick
		
		for color, carPos in carPositions.items():
			if color == self._color:
				self._carPhysics[color].Push(self._throttle, carPos.angle, carPos.pos)
			else:
				self._carPhysics[color].Push(0, carPos.angle, carPos.pos)
		
		# Check if the race has started yet
		if not self._gameOn:
			return
		
		
		# Okay we're good to give instructions!
		curVelocity = self._physics.GetCurrentVelocity()
		#print curVelocity
		if curVelocity == 0:										# Pedal to the metal!
			self.ThrottleNow(1.0)
			return											# <- Note the RETURN here
		
		#if not self._throttle == 0.65:
		#	self.Throttle(0.65)
		#
		#return									#### Testing !
		
		# Check if turbo should be activated
		if not self._outOfTrack and self._gotTurbo and self._dataCollected and not myCar.lap == 0:
			if myCar.pid in self._track.GetLongestStraight().GetRange():
				self.Turbo()
		
		
		# Check if this is the final straight
		if myCar.lap == self._numberOfLaps-1:
			finishStraight = self._track.GetFinishStraight()
			if finishStraight:
				if finishStraight.IsInside(myCar.pid):
					if not self._throttle == 1.0:
						print 'Last straight - step on it!'
						self.ThrottleNow(1.0)
				
					return									# <- Note the RETURN here
		
		# Check if lane should be switched
		if self._track.GetPiece(myCar.pid).HasSwitch():
			self._switch = 0
		else:
			direction = self._ShouldSwitch(myCar.pid, myCar.endLid)
			
			if direction != self._switch and myCar.startLid == myCar.endLid:
				if direction:
					self.Switch(direction)
				else:
					self._switch = 0
		
		
		# Check if we have sufficient data to give precise instructions
		if not self._dataCollected:
			maxAcceleration = self._physics.GetMaxEngineAcceleration()
			drag = self._physics.GetDragCoefficient()
			frictionCoefficient = self._physics.GetFrictionCoefficient()
			
			if maxAcceleration == None or drag == None: # and not self._track.GetPiece(myCar.pid).IsBend():
				if not self._throttle == 1.0:
					self.ThrottleNow(1.0)
			
			elif frictionCoefficient == None:
				bend = self._track.NextBend(myCar.pid)
				lane = self._track.GetLane(myCar.endLid + self._switch)
				maxVel = self._physics.GetApproximateMaxVelocity(bend, lane)
				curVel = self._physics.GetCurrentVelocity()
				
				if  not drag or not maxAcceleration:
					drag = -0.02
					maxAcceleration = 0.2
				
				if curVel < maxVel:
					throttle = ( maxVel - (1 - drag)*curVel )/maxAcceleration + 0.1
					self.Throttle(throttle)
				else:
					# Keep the velocity constant
					throttle = abs(drag)*(maxVel - curVel)/maxAcceleration + 0.1
					if not self._throttle == throttle:
						self.Throttle(throttle)
				
				if self._track.GetPiece(myCar.pid).IsBend():
					if self._track.GetPiece(myCar.pid-1).IsBend():
						angVel = self._physics.GetCurrentAngularVelocity()
						if abs(myCar.angle) < 1:
							if not self._throttle == 1.0:
								self.ThrottleNow(1.0)
						else:
							if not self._throttle == 0.1:
								self.Throttle(0.1)
					else:
						if abs(self._physics.GetLastPeakAngle()) > 45:
							if not self._frictionAdjusted == myCar.pid:
								self._physics.AdjustApproximateFriction(-0.01)
								self._frictionAdjusted = myCar.pid
						if abs(self._physics.GetLastPeakAngle()) > 25:
							if not self._frictionAdjusted == myCar.pid:
								self._physics.AdjustApproximateFriction(-0.02)
								self._frictionAdjusted = myCar.pid
						elif abs(self._physics.GetLastPeakAngle()) > 10:
							if not self._frictionAdjusted == myCar.pid:
								self._physics.AdjustApproximateFriction(- 0.01)
								self._frictionAdjusted = myCar.pid
						elif abs(self._physics.GetLastPeakAngle()) > 5:
							if not self._frictionAdjusted == myCar.pid:
								self._physics.AdjustApproximateFriction(+0.005)
								self._frictionAdjusted = myCar.pid
						else:
							if not self._frictionAdjusted == myCar.pid:
								self._physics.AdjustApproximateFriction(+0.01)
								self._frictionAdjusted = myCar.pid
			else:
				if self._physics.HasData():
					print 'Data collected'
					self._physics.CollectFittingData(False)
					self._dataCollected = True
		
		if not self._dataCollected:
			return											# <- Note the RETURN here
		
		# Check if a collision could happen
		self._CouldCollide(carPositions)			# This checks just for immediate collision - probably useless?
		
		# Check for possible pursuers
		if not self._qualificationRound:
			pursuer = self._CouldBeKnocked(myCar, carPositions)
			if pursuer:
				if self._CanEscapeByBreaking(myCar, pursuer):
					if not self._pursuer == pursuer.color:
						print 'Brace for impact!'
					
					if not self._throttle == 0.0:
						self.ThrottleNow(0.0)
					
					return					# We are braking in order to take the impact so no further adjustments to throttle
				
				else:
					switch = self._CanEscapeBySwitching(myCar, pursuer)
					if not switch == None:
						if not self._switch == switch:
							self.Switch(switch)
					
					else:
						if not self._pursuer == pursuer.color:						# There is nothing to do so just brace for impact
							print 'Incoming!' + pursuer.color
				
				self._pursuer = pursuer.color
			else:
				self._pursuer = None
		
		# Check for possible targets
		if not self._qualificationRound:
			if not self._track.GetPiece(myCar.pid).IsBend():
				target = self._CouldKnock(myCar, carPositions)
				if target and not self._track.GetPiece(target.pid).IsBend():			# I need better logic here, for example I should pursue by switching in accordance to the target
					if not self._throttle == 1.0:								# Also, I should make sure that the target won't escape at last moment with a switch
						self.ThrottleNow(1.0)
					
					if not self._target == target.color:
						print 'Pursuing target...' + target.color
					
					self._target = target.color
					
					return		# As we're pursuing a target there is no need to brake
				else:
					self._target = None
		
		
		# Check if there's a bend ahead and brake if neccesary
		if self._ShouldBrake(myCar):
			if not self._throttle == 0.0:
				self.ThrottleNow(0.0)
			
			return											# <- Note the RETURN here
		
		# Give instructions based on current position
		if not self._track.GetPiece(myCar.pid).IsBend():
			if not self._throttle == 1.0:
				self.ThrottleNow(1.0)							# Pedal to the metal!
		else:
			# Check if the car is slipping in a bend and adjust throttle accordingly
			angle = myCar.angle
			angVel = self._physics.GetCurrentAngularVelocity()
			angAcc = self._physics.GetCurrentAngularAcceleration()
			bend = self._track.GetBendByPid(myCar.pid)
			lane = self._track.GetLane(myCar.endLid + self._switch)
			curVelocity = self._physics.GetCurrentVelocity()
			maxAcceleration = self._physics.GetMaxEngineAcceleration()
			drag = self._physics.GetDragCoefficient()
			
			if abs(angle) < 55:
				if self._bendVelocity:
					#print self._bendVelocity
					if curVelocity < self._bendVelocity:
						throttle = ( self._bendVelocity - (1 - drag)*curVelocity )/maxAcceleration
						self.Throttle(throttle)
					else:
						# Keep the velocity constant
						throttle = abs(drag)*(self._bendVelocity - curVelocity)/maxAcceleration
						if not self._throttle == throttle:
							self.Throttle(throttle)
				else:
					if not self._throttle == 1.0:
						self.Throttle(1.0)
					
					return
					
					if curVelocity < maxVelocity:
						throttle = ( maxVelocity - (1 - drag)*curVelocity )/maxAcceleration
						self.Throttle(throttle)
					else:
						# Keep the velocity constant
						throttle = abs(drag)*(maxVelocity - curVelocity)/maxAcceleration
						if not self._throttle == throttle:
							self.Throttle(throttle)
			else:
				self.Throttle(0.0)
			return
			
			slipping = myCar.angle
			
			if curVelocity < maxVelocity:		# I should check the angular velocity here!
				self.DeltaThrottle(+0.375)
			elif abs(slipping) > 30.0:
				if self._throttle != 0:
					self.Throttle(0.0)
			elif abs(slipping) > 10.0:
				self.DeltaThrottle(-0.5)
			elif abs(slipping) > 5.0:
				self.DeltaThrottle(-0.1)
			elif abs(slipping) > 1.0:
				self.DeltaThrottle(+0.1)
			elif self._throttle < 1.0:
				self.DeltaThrottle(+0.2)
			return					# <- For Testing
			
			if maxVelocity and curVelocity < maxVelocity:		# I should check the angular velocity here!
				maxAcceleration = self._physics.GetMaxEngineAcceleration()
				dragCoefficient = self._physics.GetDragCoefficient()
				
				throttle = (maxVelocity - (1 + dragCoefficient)*curVelocity)/maxAcceleration
				self.Throttle(throttle)
			elif not self._throttle == 0.0:
				self.Throttle(0.0)
			return
	
	def _OnTurboAvailable(self, data):
		print('Turbo available!')
		self._gotTurbo = True
		self._turboTime = data['turboDurationMilliseconds']
		self._turboTicks = data['turboDurationTicks']
		self._turboFactor = data['turboFactor']
	
	def _OnTurboStart(self, data):
		if data['color'] == self._color:
			self._gotTurbo = False
		
		self._collision.SetTurboStatus(data['color'], True)
	
	def _OnTurboEnd(self, data):
		if data['color'] == self._color:
			self._physics.SetPowerFactor(1.0)
		
		self._collision.SetTurboStatus(data['color'], False)
	
	def _OnLapFinished(self, data):
		if data['car']['color'] == self._color:
			print('')
			print('Lap finished')
			print('Lap time: ' + str(data['lapTime']['millis']/1000.0))
			print('')
			print('Maximal engine acceleration: ' + str(self._physics.GetMaxEngineAcceleration()) )
			print('Drag coefficient: ' + str(self._physics.GetDragCoefficient()) )
			print('')
			print('Angular dampening: ' + str(self._physics.GetAngularDampeningCoefficient()) )
			print('Normal drag: ' + str(self._physics.GetNormalDragCoefficient()) )
			print('Tangential drag: ' + str(self._physics.GetTangentialDragCoefficient()) )
			print ('')
			print('Friction coefficient: ' + str(self._physics.GetFrictionCoefficient()) )
			print('Moment of inertia: ' + str(self._physics.GetInertiaCoefficient()) )
			print ('')

	
	def _OnCrash(self, data):
		if data['color'] == self._color:
			print('Raaahh!')
			self._outOfTrack = True			
		else:
			print('Rahrah!')
	
	def _OnSpawn(self, data):
		if data['color'] == self._color:
			print("I'm back on track!")
			self._outOfTrack = False
			self._gotTurbo = False
			self._switch = 0
			self._target = None
			self._pursuer = None
			self._bendVelocity = None
			self.ThrottleNow(1.0)
			if self._physics.GetInertiaCoefficient() < 2.5:
				self._physics.AdjustInertiaCoefficient(1.2)
			else:
				self._physics.AdjustInertiaCoefficient(1.1)
			
			if self._physics.GetFrictionCoefficient() > -0.7:
				self._physics.AdjustFrictionCoefficient(1.1)
				self._physics.AdjustInertiaCoefficient(1.1)
		else:
			print('Hrah!')
	
	def _OnGameEnd(self, data):
		print('Race ended')
		self._gameOn = False
		self.Ping()
	
	def _OnTournamentEnd(self, data):
		print('Tournament ended')
		self._physics.DumpData()
		self.Ping()
	
	def _OnError(self, data):
		print('Error: {0}'.format(data))
		self.Ping()
	
	###### Response functions ######
	
	def Join(self):
		return self._Do('join', {'name':self._name, 'key':self._key})
	
	def Throttle(self, throttle):
		if throttle < 0.0:
			throttle = 0.0
		elif throttle > 1.0:
			throttle = 1.0
		
		if not self._throttle == throttle:
			self._PutOnStack(3-throttle, 'throttle', throttle)
	
	def ThrottleNow(self, throttle):
		if throttle < 0.0:
			throttle = 0.0
		elif throttle > 1.0:
			throttle = 1.0
		
		self._PutOnStack(0, 'throttle', throttle)
	
	def DeltaThrottle(self, delta):
		throttle = self._throttle + delta
		if throttle < 0.0:
			throttle = 0.0
		elif throttle > 1.0:
			throttle = 1.0
		
		if not self._throttle == throttle:
			self.Throttle(throttle)
	
	def Switch(self, direction):
		self._PutOnStack(2, 'switchLane', direction)
	
	def Turbo(self):
		self._PutOnStack(1, 'turbo', 'Roarrr!')
	
	def Ping(self):
		self._PutOnStack(5, 'ping', {})
	
	###### Optimizing functions ######
	
	def _ShouldSwitch(self, pid, lid):
		"""Determines whether a switch should be made at the next switch piece. Returns -1 for 'Left', 1 for 'Right' or zero, if no switch is needed."""
		# This is a greedy algorithm, which takes the fastest route at one switch interval
		sid1 = self._track.NextSwitch(pid)
		sid2 = self._track.NextSwitch(sid1)	
		
		if not sid1 == None and not sid2 == None:
			# Hack for better switching
			sid1 += 1
			#sid2 += 1
			
			lengths = self._track.GetLengthsBetweenPieces(sid1,sid2)
			slen, slid = lengths[0]
			
			i = 0							# This is becoming a mess
			while i < len(lengths):
				if lengths[i][0] == slen:
					if lengths[i][1] == lid:
						slid = lid
						return 0
				i += 1
			
			if lid < slid:
				return 1
			elif lid > slid:
				return -1
			else:
				return 0
		else:
			return 0
	
	def _ShouldBrake(self, myCar):
		switchPid = self._track.NextSwitch(myCar.pid)
		curVelocity = self._physics.GetCurrentVelocity()
		maxAcceleration = self._physics.GetMaxEngineAcceleration()
		drag = self._physics.GetDragCoefficient()
		nextVelocity = curVelocity + 2*(maxAcceleration + drag*curVelocity)
		
		# Check if the car is inside a bend
		bend = self._track.NextBend(myCar.pid)
		if bend.IsInside(myCar.pid):						# Calculate the angle at the end of current bend and possibly following straight
			nextBend = self._track.NextBend(bend.GetEnd())
			dist = self._track.GetDistance(myCar.pid, bend.GetEnd()+1, myCar.endLid) - myCar.pieceDist
			t = dist/nextVelocity
			tick = int( math.floor(t) )
			maxAngle, minAngle, angle, angVel = self._physics.GetEstimateAngleOnBend(tick)
			
			
			if maxAngle > 55 or minAngle < -55:
				#print myCar.pid, bend.GetStart(), minAngle, maxAngle
				if maxAngle < 60 and minAngle > -60:
					self._bendVelocity = curVelocity
				else:
					self._bendVelocity = 0
				
				return True
			else:
				self._bendVelocity = self._physics.GetSaturationVelocity()
			
			bendVel = curVelocity
			
			dist = self._track.GetDistance(bend.GetEnd()+1, nextBend.GetStart(), myCar.endLid)
			endVel = nextVelocity + drag*dist
			
			if endVel < 0:			# The car would stop before the bend if braking now
				return False
			
			t = math.log(endVel/nextVelocity)/drag
			tick = int( math.floor(t) )
			angle, angVel = self._physics.GetEstimateAngleOnStraightAfterBend(tick, nextVelocity, angle, angVel)
		
		else:											# Calculate angle at the end of current straight
			nextBend = bend
			dist = self._track.GetDistance(myCar.pid, nextBend.GetStart(), myCar.endLid) - myCar.pieceDist
			endVel = nextVelocity + drag*dist
			bendVel = endVel
			
			if endVel < 0:			# The car would stop before the bend if braking now
				return False
			
			t = math.log(endVel/nextVelocity)/drag
			tick = int( math.floor(t) )
			angle, angVel = self._physics.GetEstimateAngleOnStraight(tick)
		
		bc = 1
		while endVel > 0 and bc < len(self._track._bends):
			bendPid = nextBend.GetStart()
			
			lid, bendLength = nextBend.GetShortestLane()
			lane = self._track.GetLane(lid)
			direction = math.copysign(1, nextBend.GetAngle())
			radius = nextBend.GetRadius() - lane.distance*direction
			bendAngle = nextBend.GetAngle()
			
			maxAngle, minAngle, angle, angVel = self._physics.GetMaxAngleOnBend(bendLength, radius, bendAngle, endVel, direction, angle, angVel)		#, angle=0, angVel=0)
			
			if maxAngle > 55 or minAngle < - 55:
				#print myCar.pid, nextBend.GetStart(), minAngle, maxAngle
				if maxAngle < 60 and minAngle > -60:
					self._bendVelocity = bendVel
				else:
					self._bendVelocity = 0
				
				return True
			
			startVel = endVel
			prevBend = nextBend
			nextBend = self._track.NextBend(prevBend.GetEnd())
			dist = self._track.GetDistance(prevBend.GetEnd(), nextBend.GetStart(), myCar.endLid)		# We are calculating on straight so the lane doesn't actually matter
			endVel = startVel + drag*dist
			
			if endVel < 0:			# The car would stop before the bend if braking now
				return False
			
			t = math.log(endVel/startVel)/drag
			tick = int( math.floor(t) )
			angle, angVel = self._physics.GetEstimateAngleOnStraightAfterBend(tick, nextVelocity, angle, angVel)
			
			bc += 1
		
		return False
				
	def _CouldCollide(self, carPositions):
		myCar = carPositions[self._color]
		for color, car in carPositions.items():
			if not color == self._color:
				if self._collision.WillCollide(myCar, car):
					print 'Collision alert!'
	
	def _CouldBeKnocked(self, me, others):
		trailer = self._collision.FindNearestBehind(me, others)
		if trailer:
			if self._collision.CanKnockOut(trailer, me):
				return trailer
			else:
				return None
		else:
			return None
	
	def _CouldKnock(self, me, others):
		trailed = self._collision.FindNearestFront(me, others)
		if trailed:
			if self._collision.CanKnockOut(me, trailed):
				return trailed
			else:
				return None
		else:
			return None
	
	def _CanEscapeByBreaking(self, me, pursuer):
		if self._collision.CanAvoidByBreaking(me, pursuer):
			return True
		else:
			return False
	
	def _CanEscapeBySwitching(self, me, pursuer):
		if self._collision.CanAvoidBySwitching(me, pursuer):
			if not me.endLid == self._switch:					# Switch should be made anyway so just stick with it
				return self._switch
			else:
				if me.endLid:			# If not on left-most lane, switch to left
					return -1
				else:				# Otherwise switch to right
					return +1
		else:						# Maybe I could use a more clever tactic here?
			return None
	
	def _FinetuneThrottle(self, posData):
		"""Finds the optimal throttle setting from the distance to the next bend."""
		pid = posData.pid
		lid = posData.endLid					# This is innaccurate as there may be a switch
		bendId = self._track.NextBend(pid)
		
		distance = self._track.GetDistance(pid, bendId, lid) - posData.pieceDist
		angle = self._track.GetPiece(bendId).GetAngle()
		arclength = self._track.GetPiece(bendId).GetLaneLength(lid)
		
		return +- 0.0
	
	###### Running functions ######
	
	def _MessageLoop(self):
		"""Main message loop for server message processing."""
		controlMap ={
			'join': self._OnJoin,
			'yourCar': self._OnYourCar,
			'gameInit': self._OnGameInit,
			'gameStart': self._OnGameStart,
			'carPositions': self._OnCarPositions,
			'turboAvailable': self._OnTurboAvailable,
			'turboStart': self._OnTurboStart,
			'turboEnd': self._OnTurboEnd,
			'lapFinished': self._OnLapFinished,
			'crash': self._OnCrash,
			'spawn': self._OnSpawn,
			'gameEnd': self._OnGameEnd,
			'tournamentEnd': self._OnTournamentEnd,
			'error': self._OnError,
		}
		
		socketFile = self._socket.makefile()
		line = socketFile.readline()
		while line:
			msg = json.loads(line)
			type, data = msg['msgType'], msg['data']
			if 'gameTick' in msg.keys():
				self._gameTick = tick = msg['gameTick']			# Maybe the tick should be passed down as argument?
			else:
				tick = None
			
			if type in controlMap.keys():
				t = time.time()
				controlMap[type](data)
				millis = (time.time() - t)*10
				if millis > 1:							# One millisecond trap for latency checks
					print 'Latency warning: ' + str(millis)
			else:
				print('Got {0}'.format(type))
			
			self.Ping()
			if type == 'gameStart' or (type == 'carPositions' and tick):
				self._ProcessStack()
			
			line = socketFile.readline()
	
	def Run(self):
		"""Runs the bot."""
		self.Join()
		self._MessageLoop()
	
	def CreateRace(self, name, key, trackName, password, carCount=1):
		data = {
					"botId":{
						"name":name,
						"key":key,
						},
					"carCount":carCount,
				}
		if 'trackName':
			data['trackName'] = trackName
		if 'password':
			data['password'] = password
		
		self._Do('createRace', data)
	
	def JoinRace(self, name, key, trackName, password, carCount=1):
		data = {
					"botId":{
						"name":name,
						"key":key,
						},
					"carCount":carCount,
				}
		if 'trackName':
			data['trackName'] = trackName
		if 'password':
			data['password'] = password
		
		self._Do('joinRace', data)




class Lane(object):
	"""Represents a lane on track."""
	def __init__(self, data):
		self.index = data['index']
		self.distance = data['distanceFromCenter']


class Piece(object):
	"""Represents a track piece."""
	_switch = False
	_bend = False
	_angle = 0
	_laneLengths = None
	
	def __init__(self, data, lanes):
		"""Initializes the piece data."""
		self._data = data
		
		if 'switch' in data.keys():
			self._switch = data['switch']
		else:
			self._switch = False
		
		if 'angle' in data.keys():
			self._bend = True
			self._angle = data['angle']
			self._radius = data['radius']
		
		self._laneLengths = {}
		
		for lane in lanes:
			id = lane.index
			length = self._CalculateLaneLength(lane)
			self._laneLengths[id] = length
	
	def _CalculateLaneLength(self, lane):
		"""Calculates the length of a lane on this piece. The argument must be a lane object."""
		if 'length' in self._data.keys():					# Straight
			length = self._data['length']
		else:									# Bend
			distance = lane.distance
			radius = self._data['radius']
			angle = self._data['angle']
			
			if angle < 0:
				laneRadius = radius + distance
				length = abs(angle/360.0*2*math.pi*laneRadius)
			else:
				laneRadius = radius - distance
				length = abs(angle/360.0*2*math.pi*laneRadius)
		
		return length
	
	def GetLaneLength(self, lid):
		"""Returns the length of the lane lid on this piece."""
		return self._laneLengths[lid]
	
	def HasSwitch(self):
		"""Returns True if lanes can be switched on this piece."""
		return self._switch
	
	def IsBend(self):
		"""Returns True if this is a bend piece."""
		return self._bend
	
	def GetAngle(self):
		"""Returns the angle of the track at this piece."""
		return self._angle
	
	def GetRadius(self):
		"""Returns the radius of this piece."""
		return self._radius


class Bend(object):
	_start = 0
	_end = 0
	_angle = 0
	_radius = 0
	_velocity = None
	_lengths = None
	
	def __init__(self, start, end, angle, radius, lengths):
		self._start = start
		self._end = end
		self._radius = radius
		self._angle = angle
		self._lengths = lengths
	
	def GetStart(self):
		return self._start
	
	def GetEnd(self):
		return self._end
	
	def GetRange(self):
		return range(self._start, self._end+1)
	
	def GetAngle(self):
		return self._angle
	
	def GetRadius(self):
		return self._radius
	
	def IsInside(self, pid):
		if pid in self.GetRange():
			return True
		else:
			return False
	
	def SetMaxVelocity(self, vel):
		self._velocity = vel
	
	def GetMaxVelocity(self):
		return self._velocity
	
	def GetLaneLength(self, lid):
		return self._lengths[lid]
	
	def GetShortestLane(self):
		length = self._lengths[0]
		lid = 0
		i = 1
		while i < len(self._lengths):
			if self._lengths[i] < length:
				length = self._lengths[i]
				lid = i
			i += 1
		
		return lid, length


class Straight(object):
	_start = 0
	_end = 0
	_totalLength = 0
	_hasFinish = False
	
	def __init__(self, start, end, length, hasFinish):
		self._start = start
		self._end = end
		self._totalLength = length
		self._hasFinish = hasFinish
	
	def GetStart(self):
		return self._start
	
	def GetEnd(self):
		return self._end
	
	def GetRange(self):
		return range(self._start, self._end+1)
	
	def GetLength(self):
		return self._totalLength
	
	def IsInside(self, pid):
		if pid in self.GetRange():
			return True
		else:
			return False
	
	def HasFinish(self):
		return self._hasFinish
	
	def GetLaneLength(self, lid):
		return self._totalLength


class Track(object):
	"""This class represents the racing0 track."""
	_pieces = None
	_lanes = None
	_bends = None
	_straights = None
	_finishStraight = None
	_hasLeftTurn = False
	_hasRightTurn = False
	
	def __init__(self, pieces, lanes):
		self._pieces = pieces
		self._lanes = sorted(lanes, key=lambda lane: lane.index)
		self._bends = bends = []
		self._straights = straights = []
		
		i = 1
		startId = 0
		endId = 0
		while i < len(pieces):
			if pieces[i].IsBend():
				if pieces[i-1].IsBend():
					if pieces[i].GetRadius() == pieces[i-1].GetRadius() and pieces[i].GetAngle()*pieces[i-1].GetAngle() > 0:	# Same bend continues
						endId = i
					else:										# Two bends are joined
						angle = self.GetTotalAngle(startId, endId)
						lengths = self.GetLaneLengths(startId, endId+1)
						radius = pieces[i-1].GetRadius()
						bend = Bend(startId, endId, angle, radius, lengths)
						bends.append(bend)
						startId = i
						endId = i
						
						if angle > 30:
							self._hasRightTurn = True
						elif angle < 30:
							self._hasLeftTurn = True
				else:											# Straight ends at a bend
					length = self.GetDistance(startId, endId+1, 0)
					straight = Straight(startId, endId, length, False)
					straights.append(straight)
					startId = i
					endId = i
			else:
				if pieces[i-1].IsBend():								# Bend ends at a straight
					angle = self.GetTotalAngle(startId, endId)
					lengths = self.GetLaneLengths(startId, endId+1)
					radius = pieces[i-1].GetRadius()
					bend = Bend(startId, endId, angle, radius, lengths)
					bends.append(bend)
					startId = i
					endId = i
					
					if angle > 30:
						self._hasRightTurn = True
					elif angle < 30:
						self._hasLeftTurn = True
				else:											# Straight continues
					endId = i
			
			i += 1
		
		if pieces[0].IsBend() == pieces[-1].IsBend():
			if pieces[0].IsBend():
				endId = bends[0].GetEnd() + len(pieces)
				angle = self.GetTotalAngle(startId, endId)
				radius = pieces[i-1].GetRadius()
				bend = Bend(startId, endId, angle, radius)
				bends.append(bend)
			else:
				endId = straights[0].GetEnd() + len(pieces)
				length = self.GetDistance(startId, endId+1, 0)
				straight = Straight(startId, endId, length, True)
				straights.append(straight)
				self._finishStraight = straight
			
	
	def PieceInBetween(self, pid, start, end):
		if start < end:
			if start <= pid <= end:
				return True
			else:
				return False
		else:
			if pid <= end or start <= pid:
				return True
			else:
				return False
	
	def GetDistance(self, pid1, pid2, lid):
		"""Calculates the distance from the beginning of piece pid1 to the beginning of piece pid2 on lane lid."""
		if pid1 > pid2:
			pid2 += len(self._pieces)
		
		i = pid1
		length = 0			# Length calculated from beginning to beginning
		while i < pid2:
			index = i % len(self._pieces)
			length += self._pieces[index].GetLaneLength(lid)
			i += 1
		
		return length
	
	def GetLaneLengths(self, pid1, pid2):
		i = 0
		lengths = []
		while i < len(self._lanes):
			length = self.GetDistance(pid1, pid2, i)
			lengths.append(length)
			i += 1
		
		return lengths
	
	def GetLengthsBetweenPieces(self, pid1, pid2):
		"""Calculates the lengths of all lanes between pieces pid1 and pid2. Returns a list of (length, laneid) pairs sorted in ascending order by the length."""
		if pid1 > pid2:
			pid2 += len(self._pieces)
		
		lengths = []
		for lane in self._lanes:
			lid = lane.index
			length = self.GetDistance(pid1, pid2, lid)
			lengths.append( (length, lid) )
		
		lengths.sort()
		
		return lengths
	
	def GetInPieceDistance(self, pos1, pos2):
		"""Finds the distance for two positions that are 'in piece'."""
		lid1, pid1, inPiece1 = pos1
		lid2, pid2, inPiece2 = pos2
		
		if pid1 > pid2:
			pid2 += len(self._pieces)
		
		if lid1 == lid2:
			dist = self.GetDistance(pid1, pid2, lid1) - inPiece1 + inPiece2
		else:
			# Seems that switching doesn't affect the distance traveled?
			dist = self.GetDistance(pid1, pid2, lid1) - inPiece1 + inPiece2
			# dist1 = self.GetDistance(pid1, pid2, lid1) - inPiece1 + inPiece2
			# dist2 = self.GetDistance(pid1, pid2, lid2) - inPiece1 + inPiece2
			# dist = (dist1 + dist2)/2.0										# EStimate the distance while switching lanes
		
		return dist
	
	def NextSwitch(self, pid):
		"""Finds the next switch piece after pid. If there are no switches in the track, this will return None."""
		i = pid+1
		while ( i % len(self._pieces) ) != pid:		# Loop through all pieces once
			index = i % len(self._pieces)
			if self._pieces[index].HasSwitch():
				return index
			
			i += 1
		
		return None
	
	def NextBend(self, pid):
		"""Finds the next bend and returns the corresponding bend object."""
		i = pid+1
		while ( i % len(self._pieces) ) != pid:		# Loop through all pieces once
			index = i % len(self._pieces)
			if self._pieces[index].IsBend():
				for bend in self._bends:
					if bend.IsInside(index):
						return bend
			
			i += 1
	
	def GetPiece(self, pid):
		"""Returns the track piece object corresponding to pid."""
		return self._pieces[pid]
	
	def GetLane(self, lid):
		"""Returns the lane object corresponding to lid."""
		return self._lanes[lid]
	
	def GetBend(self, id):
		return self._bends[id]
	
	def GetBendByPid(self, pid):
		for bend in self._bends:
			if bend.IsInside(pid):
				return bend
	
	def GetStraight(self, id):
		return self._straights[id]
	
	def GetTotalAngle(self, start, end):
		i = start
		angle = 0
		while i <= end:
			angle += self._pieces[i].GetAngle()
			i += 1
		
		return angle
	
	def GetLongestStraight(self):
		length = 0
		id = 0
		for straight in self._straights:
			if straight.GetLength() > length:
				length = straight.GetLength()
				longest = straight
			elif straight.GetLength() == length:
				if not straight.HasFinish():
					length = straight.GetLength()
					longest = straight
		
		return longest
	
	def GetFinishStraight(self):
		return self._finishStraight
	
	def HasLeftTurn(self):
		return self._hasLeftTurn
	
	def HasRightTurn(self):
		return self._hasRightTurn
	
	def NumberOfPieces(self):
		return len(self._pieces)


class Car(object):
	"""Represents a car."""
	def __init__(self, data):
		self.name = data['id']['name']
		self.color = data['id']['color']
		self.length = data['dimensions']['length']
		self.width = data['dimensions']['width']
		self.flagPos = data['dimensions']['guideFlagPosition']


class PositionData(object):
	""""Represents position data of a car."""
	def __init__(self, car, data):
		self.color = car.color
		self.length = car.length
		self.width = car.width
		self.flagPos = car.flagPos
		
		self.name = data['id']['name']
		self.color = data['id']['color']
		self.angle = data['angle']
		self.pid = data['piecePosition']['pieceIndex']
		self.pieceDist = data['piecePosition']['inPieceDistance']
		self.startLid = data['piecePosition']['lane']['startLaneIndex']
		self.endLid = data['piecePosition']['lane']['endLaneIndex']
		self.lap = data['piecePosition']['lap']
		self.pos = (self.endLid, self.pid, self.pieceDist)


class PhysicsAnalysis(object):
	"""This class is used for collection and analysis of physical data."""
	_collectFittingData = True
	_measuredLeft = False
	_measuredRight = False
	
	_position = None
	_distance = None
	_velocity = None
	_acceleration = None
	
	_throttle = None
	_deltaThrottle = None
	_enginePowerFactor = 1.0
	
	_friction = None
	_inertia = None
	_tangentialDrag = None
	_normalDrag = None
	_angularDampening = None
	_drag = None
	_maxEngineAcceleration = None
	_bendCoefficients = None
	
	_angle = None
	_angularVelocity = None
	_angularAcceleration = None
	_peakAngle = 0
	
	_approximateFriction = 0.32
	
	_decelerationCurve = None
	_accelerationCurve = None
	_driftingCurve = None
	_inertiaCurve = None
	_returningCurve = None
	_dragCurve = None
	_frictionData = None
	_bendCurves = None
	
	_estimatedAngleStraight = None
	_estimatedAngleBend = None
	_estimateCounter = 0
	
	def __init__(self, track, car):
		"""Initialize all the datasets."""
		self._track = track
		
		self._position = [(0,0,0),(0,0,0)]
		
		self._throttle = [0,0]
		self._deltaThrottle = [0,0]
		
		self._angle = [0,0]
		self._angularVelocity = [0,0]
		self._angularAcceleration = [0,0]
		
		self._normalDistance = [0,0]
		self._normalVelocity = [0,0]
		self._normalAcceleration = [0,0]
		
		self._distance = [0,0]
		self._velocity = [0,0]
		self._acceleration = [0,0]
		
		self._decelerationCurve = [[],[]]
		self._accelerationCurve = [[],[]]
		self._driftingCurve = [[],[]]
		self._inertiaCurve = [[],[]]
		self._returningCurve = [[],[]]
		self._dragCurve = [[],[]]
		self._frictionData = []
		self._bendCurves = {}
		self._bendCoefficients = {}
		
		self._estimatedAngleStraight = []
		self._estimatedAngleBend = []
		
		width = car.width
		length = car.length
		offset = length/2.0 - car.flagPos
		
		self._momentOfInertia = (1.0/12.0*(width*width + length*length) + offset*offset)/100.0
	
	def Push(self, throttle, angle, position):
		"""Push new values to datasets form throttle, angle, (lid, pid, pieceDist) and update estimates."""
		self.PushThrottle(throttle)
		self.PushAngle(angle)
		self.PushPosition(position)
		
		if self._track.GetPiece(self._position[-2][1]).HasSwitch() and not self._track.GetPiece(self._position[-1][1]).HasSwitch():
			pass		# This excludes bad data points caused by lane switching
		elif self._track.GetPiece(self._position[-3][1]).HasSwitch() and not self._track.GetPiece(self._position[-2][1]).HasSwitch():
			pass		# This excludes bad data points caused by lane switching
		else:
			self.PushDatasets()
			if self._collectFittingData:
				self.PushFittingData()
			
			#self.PushBendFittingData()
		
		self.UpdateEstimates()
		
		if self._velocity[-2] == 0 and self._velocity[-1] != 0:
			self._maxEngineAcceleration = self._acceleration[-1]
		elif self._velocity[-3] == 0 and self._acceleration[-3] == 0 and self._acceleration[-2] != 0:
			self._drag = (self._acceleration[-1] - self._acceleration[-2])/self._velocity[-2]
		
		return
		if self._angle[-1] != 0 and abs(self._angle[-2]) < 0.1 and abs(self._angle[-3]) < 0.1 and abs(self._angle[-4]) < 0.1:
			lid, pid, inPiece = self._position[-1]
			bend = self._track.GetBendByPid(pid)
			bendRadius = bend.GetRadius()
			bendAngle = bend.GetAngle()
			lane = self._track.GetLane(lid)
			radius = bendRadius - lane.distance*math.copysign(1, bendAngle)
			
			self._frictionData.append( (self._angle[-1], self._velocity[-1], radius) )
			if len(self._frictionData) == 3:
				a0, v0, r0 = self._frictionData[0]
				a1, v1, r1 = self._frictionData[1]
				a2, v2, r2 = self._frictionData[2]
				
				self._inertia = ( r0*r1*r2*( a0*(v1-v2) + a1*(v2-v0) + a2*(v0-v1) ) )  /  ( r0*r2*v1*v1*(v2-v1) + r1*r2*v0*v0*(v1-v2) + r0*r1*v2*v2*(v0-v1) )
				
				self._tangentialDrag = ( r1*r2*v0*v0*(a1-a2) + r0*r2*v1*v1*(a2-a0) + r0*r1*v2*v2*(a0-a1) )  /  ( r0*r2*v1*v1*(v2-v0) + r1*r2*v0*v0*(v1-v2) + r0*r1*v2*v2*(v0-v1) )
				
				self._friction = ( a2*r2*v1*v0*(r1*v0 - r0*v1) + a1*r1*v2*v0*(r0*v2 - r2*v0) + a0*r0*v1*v2*(r2*v1 - r1*v2) )  /  ( r0*r2*v1*v1*(v2-v0) + r1*r2*v0*v0*(v1-v2) + r0*r1*v2*v2*(v0-v1) )
				
				print self._inertia
				print self._tangentialDrag
				print self._friction
	
	def PushThrottle(self, throttle):
		"""Push throttle value to datasets."""
		self._throttle.append(throttle)
		delta = throttle - self._throttle[-2]
		self._deltaThrottle.append(delta)
	
	def PushAngle(self, angle):
		"""Push car angle to datasets."""
		self._angle.append(angle)
		
		angVel = angle - self._angle[-2]
		self._angularVelocity.append(angVel)
		
		angAcc = angVel - self._angularVelocity[-2]
		self._angularAcceleration.append(angAcc)
		
		if angVel*self._angularVelocity[-2] < 0:
			self._peakAngle = peak = max(abs(angle), abs(self._angle[-2]))
			if peak > 55:
				pass
		
		normDist = 10*math.tan(math.radians(angle))
		self._normalDistance.append(normDist)
		
		normVel = normDist - self._normalDistance[-2]
		self._normalVelocity.append(normVel)
		
		normAcc = normVel - self._normalVelocity[-2]
		self._normalAcceleration.append(normAcc)
	
	def PushPosition(self, pos):
		"""Push position data to datasets."""
		self._position.append(pos)
		
		deltaDist = velocity = self._track.GetInPieceDistance(self._position[-2], self._position[-1])
		dist = self._distance[-1] + deltaDist
		self._distance.append(dist)
		
		self._velocity.append(velocity)
		acceleration = velocity - self._velocity[-2]
		self._acceleration.append(acceleration)
	
	def PushFittingData(self):
		"""Push new data based on latest throttle, angle and position data."""
		# Record data for air drag and engine power calculation
		throttle = self._throttle[-1]
		deltaThrottle = throttle - self._throttle[-2]
		velocity = self._velocity[-2]
		acceleration = self._acceleration[-1]
		
		# This is superfluous now
		# Record data for engine power and air drag coefficient
		#if 0.00 < acceleration and throttle == 1.0 and deltaThrottle == 0:
		#	self._accelerationCurve[0].append(velocity)
		#	self._accelerationCurve[1].append(acceleration)
		#	
		#	if len(self._accelerationCurve[0]) % 10 == 0:
		#		self.FitOnThread()
		
		# Record data for friction coefficient determination
		lid, pid, pieceDist = self._position[-1]
		if self._track.GetPiece(pid).IsBend() and not self._track.GetPiece(pid).HasSwitch() and self._position[-3][1] == pid and self._position[-2][1] == pid:
			angle = self._angle[-3]
			if abs(angle) > 0:
				sinAngle = math.sin(math.radians(angle))
				cosAngle = math.cos(math.radians(angle))
				angularVelocity = self._angularVelocity[-2]
				angularAcceleration = self._angularAcceleration[-1]
				velocity = self._velocity[-2]
				
				lane = self._track.GetLane(lid)
				arclength = self._track.GetPiece(pid).GetLaneLength(lid)
				bendAngle = self._track.GetPiece(pid).GetAngle()
				bendRadius = self._track.GetPiece(pid).GetRadius()
				radius = bendRadius - lane.distance*math.copysign(1,bendAngle)  #+ 10*sinAngle*math.copysign(1, bendAngle)
				
				angDamp = self.GetAngularDampeningCoefficient()
				normDrag = self.GetNormalDragCoefficient()
				tangDrag = self.GetTangentialDragCoefficient()
				
				if angDamp and normDrag:
					if abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle  -tangDrag*velocity*cosAngle*math.copysign(1, angle)) > 0.1:
						self._driftingCurve[1].append( abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*velocity*cosAngle*math.copysign(1, angle) )  )
						self._driftingCurve[0].append( velocity*velocity/(radius + 7) )	#+friction*cosAngle*math.copysign(1, bendAngle) )		# Check these formulas
					
					#if abs( ( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*velocity*cosAngle*math.copysign(1, angle)) ) > 0.1:
					#	fsign = math.copysign(1, bendAngle) # math.copysign(1,angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*cosAngle)
					#	self._driftingCurve[0].append( abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*cosAngle*math.copysign(1, angle)) )
					#	self._driftingCurve[1].append( [ 1, velocity*velocity/radius] )	#+friction*cosAngle*math.copysign(1, bendAngle) )		# Check these formulas
					
						#if friction and inertia:
						#	self._dragCurve[0].append( abs( ( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle )/cosAngle ) - self._friction - inertia*velocity*velocity/radius )
						#	self._dragCurve[1].append( velocity )
					
					if len(self._driftingCurve[0]) % 10 == 0: # or len(self._dragCurve[0]) % 10 == 0:
						self.FitOnThread()
		
		# Record data for angular movement on straight
		elif not self._track.GetPiece(self._position[-3][1]).IsBend() and not self._track.GetPiece(self._position[-2][1]).IsBend():
			angle = self._angle[-3]
			sinAngle = math.sin(math.radians(angle))
			cosAngle = math.cos(math.radians(angle))
			tanAngle = math.tan(math.radians(angle))
			angularVelocity = self._angularVelocity[-2]
			angularAcceleration = self._angularAcceleration[-1]
			velocity = self._velocity[-2]
			
			if abs(angle) > 0 and self._angularDampening == None:						# I should probably observe for a few bends here
				self._returningCurve[1].append(angularAcceleration/angularVelocity)
				self._returningCurve[0].append(velocity*sinAngle/angularVelocity)		# This seems to be right
				
				if len(self._returningCurve[0]) % 10 == 0:
					self.FitOnThread()
	
	def PushBendFittingData(self):
		# Record data for friction coefficient determination
		lid, pid, pieceDist = self._position[-1]
		if self._track.GetPiece(pid).IsBend() and not self._track.GetPiece(pid).HasSwitch() and self._position[-3][1] == pid and self._position[-2][1] == pid:
			angle = self._angle[-3]
			if abs(angle) > 0:
				sinAngle = math.sin(math.radians(angle))
				cosAngle = math.cos(math.radians(angle))
				angularVelocity = self._angularVelocity[-2]
				angularAcceleration = self._angularAcceleration[-1]
				velocity = self._velocity[-2]
				
				lane = self._track.GetLane(lid)
				arclength = self._track.GetPiece(pid).GetLaneLength(lid)
				bendAngle = self._track.GetPiece(pid).GetAngle()
				bendRadius = self._track.GetPiece(pid).GetRadius()
				radius = bendRadius - lane.distance*math.copysign(1,bendAngle)  #+ 10*sinAngle*math.copysign(1, bendAngle)
				
				angDamp = self.GetAngularDampeningCoefficient()
				normDrag = self.GetNormalDragCoefficient()
				tangDrag = self.GetTangentialDragCoefficient()
				
				if radius in self._bendCurves.keys():
					curve = self._bendCurves[radius]
				else:
					curve = [[], []]
					self._bendCurves[radius] = curve
				
				if angDamp and normDrag and tangDrag and len(curve[0]) < 20:
					if abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle  -tangDrag*velocity*cosAngle*math.copysign(1, angle)) > 0.1:
						curve[1].append( abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*velocity*cosAngle*math.copysign(1, angle) )  )
						curve[0].append( velocity*velocity/radius )	#+friction*cosAngle*math.copysign(1, bendAngle) )		# Check these formulas
						
						if len(curve[0]) % 5 == 0:
							self.FitBendCurve(radius, curve)
	
	def PushDatasets(self):
		"""Push new data based on latest throttle, angle and position data."""
		# Record data for friction coefficient determination
		lid, pid, pieceDist = self._position[-1]
		if self._track.GetPiece(pid).IsBend() and not self._track.GetPiece(pid).HasSwitch() and self._position[-3][1] == pid and self._position[-2][1] == pid and self._deltaThrottle[-1] == 0 and self._deltaThrottle[-2] == 0:
			angle = self._angle[-3]
			if abs(angle) > 0:
				sinAngle = math.sin(math.radians(angle))
				cosAngle = math.cos(math.radians(angle))
				angularVelocity = self._angularVelocity[-2]
				angularAcceleration = self._angularAcceleration[-2]
				velocity = self._velocity[-2]
				
				lane = self._track.GetLane(lid)
				arclength = self._track.GetPiece(pid).GetLaneLength(lid)
				bendAngle = self._track.GetPiece(pid).GetAngle()
				bendRadius = self._track.GetPiece(pid).GetRadius()
				radius = bendRadius - lane.distance*math.copysign(1,bendAngle)  #+ 10*sinAngle*math.copysign(1, bendAngle)
				
				angDamp = self.GetAngularDampeningCoefficient()
				normDrag = self.GetNormalDragCoefficient()
				tangDrag = self.GetTangentialDragCoefficient()
				
				pid = self._position[-1][1]
				
				if angDamp != None and normDrag != None: # and self._track._bends[2].IsInside(pid):
					#if True: #angularAcceleration*bendAngle > 0 and angularVelocity*bendAngle > 0 and angle*bendAngle > 0:
					friction = 0.0 #self.GetFrictionCoefficient()
					inertia = 1.0 #self.GetInertiaCoefficient()
					if friction != None and inertia != None and tangDrag != None:
						if abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle  -tangDrag*velocity*cosAngle*math.copysign(1, angle)) > 0.1:
							self._inertiaCurve[1].append( abs( angularAcceleration -angDamp*angularVelocity -normDrag*velocity*sinAngle -tangDrag*velocity*cosAngle*math.copysign(1, angle) ) )
							self._inertiaCurve[0].append( velocity*velocity/(radius + 7))	#+friction*cosAngle*math.copysign(1, bendAngle) )		# Check these formulas
		
		elif not self._track.GetPiece(self._position[-3][1]).IsBend() and not self._track.GetPiece(self._position[-2][1]).IsBend() and self._enginePowerFactor == 1.0:
			angle = self._angle[-3]
			sinAngle = math.sin(math.radians(angle))
			cosAngle = math.cos(math.radians(angle))
			angularVelocity = self._angularVelocity[-2]
			angularAcceleration = self._angularAcceleration[-1]
			velocity = self._velocity[-2]
			
			if abs(angle) > 0:
				angDamp = self.GetAngularDampeningCoefficient()
				normDrag = self.GetNormalDragCoefficient()
				tangDrag = self.GetTangentialDragCoefficient()
				if angDamp and normDrag:
					self._decelerationCurve[1].append(angularAcceleration/angularVelocity)
					self._decelerationCurve[0].append(velocity*sinAngle/angularVelocity)
	
	def FitOnThread(self):
		threading.Thread(target=self.FitParameters).start()
	
	def FitParameters(self):
		try:
			#self._drag, self._maxEngineAcceleration = self.FitAccelerationForces()
			self._angularDampening, self._normalDrag, self._tangentialDrag = self.FitReturningForces()
			self._friction, self._inertia = self.FitBendForces()
			
			#self._tangentialDrag = self.FitDrag()
		except IndexError:
			pass
		
		return
		print('')
		print('Maximal engine acceleration: ' + str(self.GetMaxEngineAcceleration()) )
		print('Drag coefficient: ' + str(self.GetDragCoefficient()) )
		print('')
		print('Angular dampening: ' + str(self.GetAngularDampeningCoefficient()) )
		print('Normal drag: ' + str(self.GetNormalDragCoefficient()) )
		print('Tangential drag: ' + str(self.GetTangentialDragCoefficient()) )
		print ('')
		print('Friction coefficient: ' + str(self.GetFrictionCoefficient()) )
		print('Moment of inertia: ' + str(self.GetInertiaCoefficient()) )
		print ('')
	
	def FitAccelerationForces(self):
		#return -0.02, 0.2
		if len(self._accelerationCurve[0]) >= 10:
			v, a = self._accelerationCurve
			drag, maxAcceleration = fitting.FindFit(v,a)
			return drag, maxAcceleration
		else:
			return None, None
	
	def FitReturningForces(self):
		#return -0.11, -0.074, 0.0
		# Seems that these figures aren't varied in the simulation?
		if len(self._returningCurve[0]) >= 20:
			x, y = self._returningCurve
			normDrag, angVelDrag = fitting.FindFit(x,y)
			tangDrag = 0
			return angVelDrag, normDrag, tangDrag
		else:
			return None, None, None
	
	def FitBendForces(self):
		#return -1.0, 3.0
		if len(self._driftingCurve[0]) >= 20:
			x, y = self._driftingCurve
			inertia, friction = fitting.FindFit(x,y)
			return friction, inertia
		else:
			return None, None
	
	def FitBendCurve(self, radius, curve):
		x, y = self._driftingCurve
		inertia, friction = fitting.FindFit(x,y)
		
		self._bendCoefficients[radius] = (inertia, friction)
	
	def FitDrag(self):
		if len(self._dragCurve) >= 20:
			tangDrag, err = fitting.LinReg(self._dragCurve)
			return tangDrag
			
			#friction, err, points = fitting.CleanedAverage(self._dragCurve)
			#if points >= 30:
			#	return friction
			#else:
			#	return None
		else:
			return None
	
	def UpdateEstimates(self):
		if self._track.GetPiece(self._position[-1][1]).IsBend() and self._estimatedAngleBend:
			ang, vel = self._estimatedAngleBend.pop(0)
			if abs(ang - self._angle[-1]) > 0.5:			# Estimate is drifting from data
				#if self._acceleration[-1] < 0.001:
					#print 'Estimate is drifting: bend ' + str(self._estimateCounter)
					#print str(ang) + '\t\t' + str(self._angle[-1])
				self._estimatedAngleBend = self.CalculateAngleOnBend(len(self._estimatedAngleBend))
				self._estimateCounter = 0
			else:
				self._estimateCounter += 1
		elif self._estimatedAngleStraight:
			ang, vel = self._estimatedAngleStraight.pop(0)
			if abs(ang - self._angle[-1]) > 0.5:			# Estimate is drifting from data
				#print 'Estimate is drifting: straight ' + str(self._estimateCounter)
				self._estimatedAngleStraight = self.CalculateAngleOnStraight(len(self._estimatedAngleBend))
				self._estimateCounter = 0
			else:
				pass
				#self._estimateCounter += 1
		
	
	###### Public functions ######
	
	def CollectFittingData(self, flag=True):
		self._collectFittingData = flag
		self.FitOnThread()
	
	def HasData(self):
		if self._friction == None:
			return False
		elif self._inertia == None:
			return False
		elif self._tangentialDrag == None:
			return False
		elif self._normalDrag == None:
			return False
		elif self._angularDampening == None:
			return False
		elif self._drag == None:
			return False
		elif self._maxEngineAcceleration == None:
			return False
		else:
			return True
	
	def Flush(self):
		self._position = [(0,0,0),(0,0,0)]
		self._throttle = [0,0]
		self._deltaThrottle = [0,0]
		self._angle = [0,0]
		self._angularVelocity = [0,0]
		self._angularAcceleration = [0,0]
		self._normalDistance = [0,0]
		self._normalVelocity = [0,0]
		self._normalAcceleration = [0,0]
		self._distance = [0,0]
		self._velocity = [0,0]
		self._acceleration = [0,0]
	
	def FlushFrictionData(self):
		self._dragCurve = []
	
	def GetCurrentVelocity(self):
		"""Returns current car velocity."""
		return self._velocity[-1]
	
	def GetMeanVelocity(self, ticks):
		"""Returns car mean velocity from last tick ticks."""
		if len(self._distance) > ticks:
			return (self._distance[-1] - self._distance[-1-ticks])/ticks
		else:
			return (self._distance[-1] - self._distance[0])/len(self._distance)
	
	def GetCurrentAcceleration(self):
		"""Returns current car acceleration."""
		return self._acceleration[-1]
	
	def GetMeanAcceleration(self, ticks):
		"""Returns car mean acceleration from last tick ticks."""
		if len(self._acceleration) > ticks:
			return (self._acceleration[-1] - self._acceleration[-1-ticks])/ticks
		else:
			return (self._acceleration[-1] - self._acceleration[0])/len(self._acceleration)
	
	def GetCurrentAngle(self):
		"""Returns current car angle."""
		return self._angle[-1]
	
	def GetLastPeakAngle(self):
		return self._peakAngle
	
	def GetCurrentAngularVelocity(self):
		"""Returns current angular velocity of the car."""
		return self._angularVelocity[-1]
	
	def GetCurrentAngularAcceleration(self):
		"""Returns current angular acceleration of the car."""
		return self._angularVelocity[-1]
	
	def GetMaxVelocity(self):
		"""Returns the maximum velocity so far."""
		return max(self._velocity)
	
	def GetMinVelocity(self):
		"""Returns the minimum velocity so far."""
		return min(self._velocity)
	
	def GetMaxAcceleration(self):
		"""Returns the maximum acceleration so far."""
		return max(self._acceleration)
	
	def GetMaxDeceleration(self):
		"""Returns the maximum deceleration so far."""
		return min(self._acceleration)
	
	def GetInertiaCoefficient(self):
		return self._inertia
	
	def GetFrictionCoefficient(self):
		"""Returns the friction coefficient."""
		return self._friction
	
	def GetMaxEngineAcceleration(self):
		"""Returns the engine acceleration with current power factor."""
		if self._maxEngineAcceleration:
			return self._enginePowerFactor*self._maxEngineAcceleration
		else:
			return None
	
	def GetBareMaxEngineAcceleration(self):
		"""Returns the 'bare' engine acceleration ie. without current power factor."""
		if self._maxEngineAcceleration:
			return self._maxEngineAcceleration
		else:
			return None
	
	def GetDragCoefficient(self):
		"""Returns the air drag coefficient for tangential velocity."""
		return self._drag
	
	def GetAngularDampeningCoefficient(self):
		"""Returns the angular dampening coefficient."""
		return self._angularDampening
	
	def GetNormalDragCoefficient(self):
		"""Returns the drag coefficient for angle-dependent normal-drag."""
		return self._normalDrag
	
	def GetTangentialDragCoefficient(self):
		"""Returns the drag coefficient for angle-dependent tangential-drag."""
		return self._tangentialDrag
	
	def CalculateAngleOnBend(self, ticks):
		"""Calculates the estimated angle assuming that the velocity stays constant."""
		estAng = []
		
		angDamp = self._angularDampening
		normDrag = self._normalDrag
		tangDrag = self._tangentialDrag
		friction = self._friction
		inertia = self._inertia
		drag = self._drag
		
		lid, pid, pieceDist = self._position[-1]
		bend = self._track.GetPiece(pid)
		bendRadius = bend.GetRadius()
		bendAngle = bend.GetAngle()
		lane = self._track.GetLane(lid)
		direction = math.copysign(1, bendAngle)
		radius = bendRadius - lane.distance*direction
		
		angle = self._angle[-2]
		cosAngle = math.cos(math.radians(angle))
		sinAngle = math.sin(math.radians(angle))
		angVel = self._angularVelocity[-1]
		vel = self._velocity[-1]
		
		fsign = math.copysign(1, bendAngle) # math.copysign(1, inertia*vel*vel/radius*math.copysign(1,bendAngle) +tangDrag*vel*math.copysign(1, angle))
		
		radius += 7
		
		if inertia*vel*vel/radius + friction < 0:
			if angle == 0.0 and angVel == 0.0:
				for i in range(ticks+1):
					estAng.append((0,0))
			else:
				estAng = self.CalculateAngleOnStraight(ticks)
		else:
			angAcc = angDamp*angVel + normDrag*vel*sinAngle + tangDrag*vel*cosAngle*math.copysign(1, angle) + ( inertia*vel*vel/radius + friction )*direction
			
			t = 0
			while t <= ticks:
				angle += angVel
				cosAngle = math.cos(math.radians(angle))
				sinAngle = math.sin(math.radians(angle))
				angVel += angAcc
				#fsign = math.copysign(1, vel*vel/radius*direction +tangDrag*vel*math.copysign(1, angle))
				angAcc = angDamp*angVel + normDrag*vel*sinAngle + tangDrag*vel*cosAngle*math.copysign(1, angle) + ( inertia*vel*vel/radius + friction )*direction
				estAng.append((angle, angVel))
				
				t += 1
		
		estAng.pop(0)
		return estAng
	
	def GetEstimateAngleOnBend(self, tick):
		if tick <=0:
			return self._angle[-1], self._angle[-1], self._angle[-1], self._angularVelocity[-1]
		elif not self._estimatedAngleBend:
			self._estimatedAngleBend = self.CalculateAngleOnBend(tick)
		elif not tick <= len(self._estimatedAngleStraight):
			self._estimatedAngleBend = self.CalculateAngleOnBend(tick)
		
		try:
			angle, angVel = self._estimatedAngleBend[tick-1]
			maxAngle = max(self._estimatedAngleBend)[0]
			minAngle = min(self._estimatedAngleBend)[0]
			return maxAngle, minAngle, angle, angVel
		except IndexError:									# Joint of two bends?
			return self._angle[-1], self._angle[-1], self._angle[-1], self._angularVelocity[-1]
	
	def CalculateAngleOnStraight(self, ticks):
		"""Calculates the estimated angle assuming that the car is breaking."""
		estAng = []
		
		angDamp = self._angularDampening
		normDrag = self._normalDrag
		drag = self._drag
		
		angle = self._angle[-2]
		angVel = self._angularVelocity[-1]
		vel = self._velocity[-1]					# This isn't exactly good approximation...
		acc = drag*vel
		angAcc = angDamp*angVel + normDrag*vel*math.sin(math.radians(angle))
		
		t = 0
		while t <= ticks:
			angle += angVel
			angVel += angAcc
			angAcc = angDamp*angVel + normDrag*vel*math.sin(math.radians(angle))
			estAng.append((angle, angVel))
			
			vel += acc
			acc = drag*vel
			
			t += 1
		
		return estAng
	
	def GetEstimateAngleOnStraight(self, tick):
		if tick <=0:
			return self._angle[-1], self._angularVelocity[-1]
		elif not self._estimatedAngleStraight:
			self._estimatedAngleStraight = self.CalculateAngleOnStraight(tick)
		elif not tick <= len(self._estimatedAngleStraight):
			self._estimatedAngleStraight = self.CalculateAngleOnStraight(tick)
		
		return self._estimatedAngleStraight[tick-1]
	
	def GetEstimateAngleOnStraightAfterBend(self, ticks, vel, angle, angVel):
		"""Calculates the estimated angle assuming that the car is breaking with some starting angle and angular velocity."""
		estAng = []
		
		angDamp = self._angularDampening
		normDrag = self._normalDrag
		drag = self._drag
		
		acc = drag*vel
		angAcc = angDamp*angVel + normDrag*vel*math.sin(math.radians(angle))
		
		t = 0
		while t <= ticks:
			angle += angVel
			angVel += angAcc
			angAcc = angDamp*angVel + normDrag*vel*math.sin(math.radians(angle))
			estAng.append((angle, angVel))
			
			vel += acc
			acc = drag*vel
			
			t += 1
		
		return estAng[-1]
	
	def GetMaxAngleOnBend(self, length, radius, bendAngle, vel, direction, angle=0, angVel=0):
		ticks = int(math.ceil(length/vel))
		
		if ticks <= 0:
			return angle, angle, angle, angVel
		else:
			ticks += 1
		
		angDamp = self._angularDampening
		normDrag = self._normalDrag
		tangDrag = self._tangentialDrag
		drag = self._drag
		friction = self._friction
		inertia = self._inertia
		
		cosAngle = math.cos(math.radians(angle))
		sinAngle = math.sin(math.radians(angle))
		
		fsign = math.copysign(1, bendAngle) # math.copysign(1, inertia*vel*vel/radius*math.copysign(1,bendAngle) +tangDrag*vel*math.copysign(1, angle))
		
		radius += 7
		
		angEst = [(angle,angVel)]
		if inertia*vel*vel/radius + friction < 0:
			angEst = self.CalculateAngleOnStraight(ticks)
		else:
			angAcc = angDamp*angVel + normDrag*vel*sinAngle + tangDrag*vel*cosAngle*math.copysign(1, angle) + ( inertia*vel*vel/radius + friction )*direction
			t = 0
			while t <= ticks:
				angle += angVel
				cosAngle = math.cos(math.radians(angle))
				sinAngle = math.sin(math.radians(angle))
				angVel += angAcc
				#fsign = math.copysign(1, inertia*vel*vel/radius*math.copysign(1,bendAngle) +tangDrag*vel*math.copysign(1, angle))
				angAcc = angDamp*angVel + normDrag*vel*sinAngle + tangDrag*vel*cosAngle*math.copysign(1, angle) + ( inertia*vel*vel/radius + friction )*direction
				angEst.append((angle, angVel))
				
				if abs(angle) >  60:
					break
				
				t += 1
		
		return max(angEst)[0], min(angEst)[0], angEst[-1][0], angEst[-1][1]
	
	def GetApproximateMaxVelocity(self, bend, lane):
		"""Returns the maximal tangential velocity based on rough approximation."""
		angle = bend.GetAngle()
		bendRadius = bend.GetRadius()
		radius = bendRadius - math.copysign(1, angle)*lane.distance
		friction = self._approximateFriction
		
		maxVel = math.sqrt(friction*radius)		# This is a rough approximation
		
		return maxVel
	
	def AdjustApproximateFriction(self, delta):
		self._approximateFriction += delta
	
	def AdjustFrictionCoefficient(self, factor):
		if factor > 0 and self._friction != None:
			self._friction = factor*self._friction
	
	def AdjustInertiaCoefficient(self, factor):
		if factor > 0 and self._inertia != None:
			self._inertia = factor*self._inertia
	
	def GetMaxVelocityAtDistance(self, dist, pid, lid):
		maxVel = self.GetMaxTangentialVelocity(pid, lid)
		drag = self.GetDragCoefficient()
		if drag and maxVel:
			nextVel = maxVel - drag*dist
			return nextVel
		else:
			return 0
	
	def GetBreakingDistance(self, bend, lane, curVel, peakAngle=None, peakDirection=None):
		"""Returns the breaking distance needed to stay on track at the bend pid on lane lid when current velocity is curVel."""
		maxVel = self.GetMaxTangentialVelocity(bend, lane, peakAngle, peakDirection)
		drag = self.GetDragCoefficient()
		if drag and maxVel:
			distance = (maxVel - curVel)/drag
			return distance
		else:
			return 0
	
	def GetSaturationVelocity(self):
		return abs(self.GetMaxEngineAcceleration()/self._drag)
	
	def SetPowerFactor(self, factor):
		self._enginePowerFactor = factor
	
	def DumpData(self):
		"""Dumps all collected data to log-files on disk."""
		import os, time
		try:						# Tries to chdir to a dump dir
			os.chdir('dumps')
		except:					# Game server doesn't have this so this will exit without dumping data
			return
		
		dumpDir = 'analyze'
		# dumpDir = time.strftime('%d_%H.%M')
		# os.mkdir(dumpDir)
		os.chdir(dumpDir)
		
		f = open('throttle.log', 'w')
		for value in self._throttle:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('deltaThrottle.log', 'w')
		for value in self._deltaThrottle:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('angle.log', 'w')
		for value in self._angle:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('angularVelocity.log', 'w')
		for value in self._angularVelocity:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('angularAcceleration.log', 'w')
		for value in self._angularAcceleration:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('distance.log', 'w')
		for value in self._distance:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('velocity.log', 'w')
		for value in self._velocity:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('acceleration.log', 'w')
		for value in self._acceleration:
			f.write(str(value)+'\n')
		f.close()
		
		f = open('decelerationCurve.log', 'w')
		v,a = self._decelerationCurve
		i = 0
		while i < len(v):
			f.write(str(v[i])+'\t'+str(a[i])+'\n')
			i += 1
		f.close()
		
		f = open('accelerationCurve.log', 'w')
		t,a = self._accelerationCurve
		i = 0
		while i < len(t):
			f.write(str(t[i])+'\t'+str(a[i])+'\n')
			i += 1
		f.close()
		
		f = open('driftingCurve.log', 'w')
		x,y = self._driftingCurve
		i = 0
		while i < len(x):
			f.write(str(x[i])+'\t'+str(y[i])+'\n')
			i += 1
		f.close()
		
		f = open('inertiaCurve.log', 'w')
		x,y = self._inertiaCurve
		i = 0
		while i < len(x):
			f.write(str(x[i])+'\t'+str(y[i])+'\n')
			i += 1
		f.close()
		
		f = open('returningCurve.log', 'w')
		x,y = self._returningCurve
		i = 0
		while i < len(x):
			f.write(str(x[i])+'\t'+str(y[i])+'\n')
			i += 1
		f.close()
		
		f = open('frictionCurve.log', 'w')
		for value in self._dragCurve:
			f.write(str(value)+'\n')
		f.close()


class CollisionAnalysis(object):
	_physics = None
	_track = None
	_turbo = None
	
	def __init__(self, botColor, carPhysics, track):
		self._botColor = botColor
		self._physics = carPhysics
		self._track = track
		
		self._turbo = {}
		for color in carPhysics.keys():
			self._turbo[color] = False
	
	def _IsBehind(self, car1, car2):
		"""Returns True if car1 is behind car two ie. distance in track measured from car1 to car2 is shorter than distance measured from car2 to car1."""
		if car1.pid < car2.pid:
			if car2.pid - car1.pid < car1.pid - car2.pid + self._track.NumberOfPieces():
				return True
			else:
				return False
		elif car2.pid < car1.pid:
			if car1.pid - car2.pid < car2.pid - car1.pid + self._track.NumberOfPieces():
				return False
			else:
				return True
		else:					# Cars are on the same piece
			if car1.pieceDist < car2.pieceDist:
				return True
			else:
				return False
	
	def _GetTicks(self, dist, vel, maxEndVel):
		maxA = self._physics[self._botColor].GetMaxEngineAcceleration()
		drag = self._physics[self._botColor].GetDragCoefficient()
		
		s = 0
		v = vel
		a = maxA + drag*vel
		ticks = 0
		while s < dist and v > maxEndVel:
			s += v
			v += a
			if dist - s > (maxEndVel - v)/drag:	# ?
				a = maxA + drag*v
			else:
				a = drag*v
		
		return ticks
	
	def _GetDistance(self, ticks, vel):
		maxA = self._physics[self._botColor].GetMaxEngineAcceleration()
		drag = self._physics[self._botColor].GetDragCoefficient()
		
		s = 0
		v = vel
		a = maxA + drag*vel
		t = 0
		while t < ticks:
			s += v
			v += a
			
			t += 1
		
		return s
	
	def _FindCollisionPoint(self, car1, car2):
		"""Finds the collision point when car1 is breaking and car2 speeding, and car2 is behind car 1."""
		if self._IsBehind(car1, car2):
			raise ValueError
		
		maxA = self._physics[self._botColor].GetMaxEngineAcceleration()
		drag = self._physics[self._botColor].GetDragCoefficient()
		diff = self._track.GetInPieceDistance(car2.pos, car1.pos)
		
		s1 = 0
		v1 = self.GetCurrentVelocity(car1)
		a1 = drag*v1
		
		s2 = - diff
		v2 = self.GetCurrentVelocity(car1)
		a2 = maxA + drag*v2
		
		while s2 < s1:
			s1 += v1
			v1 += a1
			
			s2 += v2
			v2 += a2
		
		return s1, v2
	
	def GetCurrentVelocity(self, car):
		return self._physics[car.color].GetCurrentVelocity()
	
	def WillCollide(self, car1, car2):
		if not car1.endLid == car2.endLid:
			return False
		else:
			vel1 = self.GetCurrentVelocity(car1)
			vel2 = self.GetCurrentVelocity(car2)
			if self._IsBehind(car1, car2):
				distance = self._track.GetInPieceDistance(car1.pos, car2.pos) - car1.flagPos + car2.length - car2.flagPos
				deltaDist = vel2 - vel1
			else:
				distance = self._track.GetInPieceDistance(car2.pos, car1.pos) - car2.flagPos + car1.length - car1.flagPos
				deltaDist = vel1 - vel2
			
			if distance + deltaDist <= 0:
				return True
			else:
				return False
	
	def FindNearestFront(self, car, others):
		nearest = None
		for other in others.values():
			if not other == car:
				if self._IsBehind(car, other):
					if nearest:
						if self._IsBehind(other, nearest):
							nearest = other
					else:
						nearest = other
		
		return nearest
	
	def FindNearestBehind(self, car, others):
		nearest = None
		for other in others.values():
			if not other == car:
				if self._IsBehind(other, car):
					if nearest:
						if self._IsBehind(nearest, other):
							nearest = other
					else:
						nearest = other
		
		return nearest
	
	def CanKnockOut(self, car1, car2):
		if not car1.endLid == car2.endLid:
			return False
		elif self._IsBehind(car2, car1):
			return False
		else:
			return False
			
			vel1 = self.GetCurrentVelocity(car1)
			vel2 = self.GetCurrentVelocity(car2)
			
			bend = self._track.NextBend(car1.pid)
			if bend.IsInside(car2.pid):
				return False
			
			bendPid = bend.GetStart()
			lane = self._track.GetLane(car2.endLid)
			maxVel = self._physics[self._botColor].GetMaxTangentialVelocity(bend, lane)*1.05		# Take 5% error marginal for the end velocity
			bendDist = self._track.GetInPieceDistance(car2.pos, (car2.endLid, bendPid, 0))
			ticksToBend = self._GetTicks(bendDist, vel2, maxVel)
			distTraveled = self._GetDistance(ticksToBend, vel1)
			
			if distTraveled < bendDist:
				return False
			else:
				return True
	
	def CanAvoidByBreaking(self, car1, car2):
		vel1 = self.GetCurrentVelocity(car1)
		vel2 = self.GetCurrentVelocity(car2)
		bend = self._track.NextBend(car1.pid)
		bendPid = bend.GetStart()
		lane = self._track.GetLane(car1.endLid)
		bendDist = self._track.GetInPieceDistance(car1.pos, (car1.endLid, bendPid, 0))
		breakingDist = self._physics[self._botColor].GetBreakingDistance(bend, lane, vel1)
		
		if bendDist < breakingDist:
			return False
		else:
			collDist, collVel = self._FindCollisionPoint(car1, car2)
			return False
	
	def CanAvoidBySwitching(self, car1, car2):
		sid = self._track.NextSwitch(car1.pid)
		switchDist = self._track.GetInPieceDistance(car1.pos, (car1.endLid, sid, 0))
		collDist, collVel = self._FindCollisionPoint(car1, car2)
		if switchDist < collDist:
			return True								# Of course the pursuer can also switch so this is a last resort
		else:
			return False
	
	def SetTurboStatus(self, color, status):
		self._turbo[color] = status


class DummyBot(object):
	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send(msg + "\n")

	def join(self):
		return self.msg("join", {
							"name": self.name,
							"key": self.key
							})

	def throttle(self, throttle):
		self.msg("throttle", throttle)

	def ping(self):
		self.msg("ping", {})

	def run(self):
		self.join()
		self.msg_loop()

	def on_join(self, data):
		print("Joined")
		self.ping()

	def on_game_start(self, data):
		print("Race started")
		self.ping()

	def on_car_positions(self, data):
		self.throttle(0.5)

	def on_crash(self, data):
		print("Someone crashed")
		self.ping()

	def on_game_end(self, data):
		print("Race ended")
		self.ping()

	def on_error(self, data):
		print("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			line = socket_file.readline()




if __name__ == "__main__":
	if len(sys.argv) != 5:
		print('Usage: ./run host port botname botkey')
	else:
		host, port, name, key = sys.argv[1:5]
		print('Connecting with parameters:')
		print('host={0}, port={1}, bot name={2}, key={3}'.format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = BaseBot(s, name, key)
		bot.Run()








