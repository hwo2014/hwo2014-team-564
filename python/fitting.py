import math

from matlib import transpose, mattmat, vec2colmat, mat2vec, matdim, matprint 
from qr import  qr

def BSub(r, z):
	""" solves "R b = z", where r is triangular"""
	m, n = matdim(r)
	p, q = matdim(z)
	b = [[0] * n]
	pp, qq = matdim(b)
	
	for j in range(n-1, -1, -1):
		zz = z[0][j] - sum(r[j][k]*b[0][k] for k in range(j+1, n))
		b[0][j] = zz / r[j][j]
	
	return b

def MultiLinReg(y, x):
	# QR decomposition
	q, r = qr(x)
	
	# z = Q^T y
	z = mattmat(q, vec2colmat(y))
	
	# back substitute to find b in R b = z
	b = BSub(r, transpose(z))
	b = b[0]
	
	return b

def Average(values):
	"""Calculates the average of values."""
	sum = 0
	for x in values:
		sum += x
	
	avg = sum/len(values)
	
	return avg

def ProductAverage(values1, values2):
	"""Calculates the product average of two lists."""
	if not len(values1) == len(values2):
		raise ValuError, "Length of lists must match"
	
	sum = 0
	i = 0
	while i < len(values1):
		x,y = values1[i], values2[i]
		sum += x*y
		i += 1
	
	avg = sum/len(values1)
	
	return avg

def StandardDeviation(values):
	"""Returns the standard deviation of values."""
	avg = Average(values)
	prodAvg = ProductAverage(values, values)
	variance = prodAvg - avg*avg
	
	return math.sqrt(variance)

def CleanedAverage(values):
	"""Returns a cleaned average taken from values differing from the original average by less than standard deviation."""
	avg = Average(values)
	dev = StandardDeviation(values)
	
	cleaned = []
	for x in values:
		if x < avg and avg - x < 2*dev:
			cleaned.append(x)
		elif x > avg and x - avg < dev/2.0:
			cleaned.append(x)
	
	cleanAvg = Average(cleaned)
	
	cleanDev = StandardDeviation(cleaned)
	meanError = cleanDev/math.sqrt(len(cleaned))
	
	return cleanAvg, meanError, len(cleaned)

def LinReg(x,y):
	"""Fits the data sets x, y with a linear regression to y = b x + a. Returns the coefficients b, a."""
	b = ( ProductAverage(x, y) - Average(x)*Average(y) )/( ProductAverage(x,x) - Average(x)*Average(x) )
	a = Average(y) - b*Average(x)
	
	return b, a

def FindFit(x,y):
	"""Find linear fit for two datasets."""
	return LinReg(x,y)